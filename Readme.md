# Sliding Puzzle
A sliding puzzle game written in JavaScript using ReactJS and Parcel.

# Demo
[http://nifty-meninsky-7880a9.netlify.com](http://nifty-meninsky-7880a9.netlify.com/)

Image courtesy of [MediaMonks](https://www.mediamonks.com/).

# Features
- Use the arrow keys on your keyboard to move a tile.
- Each puzzle has a unique sharable link.

# Development
```
npm install
npm start
```

# Build
```
npm install
npm run build
```
