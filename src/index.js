import React from 'react'
import ReactDOM from 'react-dom'
import SlidingPuzzle from './view/'
import style from './index.css'
import src from './assets/monks.jpg'

ReactDOM.render(<SlidingPuzzle rows={3} columns={3} src={src}/>, root)
