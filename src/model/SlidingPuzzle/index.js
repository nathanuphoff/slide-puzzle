import Hashids from 'hashids'

const hashids = new Hashids('SlidingPuzzle', 8)

export default function SlidingPuzzle(rows = 3, columns = rows) {
  this.size = rows * columns
  this.rows = rows
  this.columns = columns
  this.tiles = init(this.size)
  this.hole = this.size - 1
}

SlidingPuzzle.prototype.encode = function(index) {
  return hashids.encode(this.tiles.map(value => value + 1).join(''))
}

SlidingPuzzle.prototype.decode = function(hash) {
  return hashids.decode(hash).toString().split('').map(value => parseInt(value) - 1)
}

SlidingPuzzle.prototype.isValidHash = function(hash) {
  const tiles = this.decode(hash).sort()
  return tiles.length === this.size
    && tiles.every((value, index) => value === index)
}

SlidingPuzzle.prototype.load = function(hash) {
  return this.set(this.decode(hash))
}

SlidingPuzzle.prototype.reset = function() {
  return this.set(init(this.size))
}

SlidingPuzzle.prototype.set = function(tiles) {
  this.tiles = tiles
  this.hole = tiles.indexOf(this.size - 1)
  return this
}

SlidingPuzzle.prototype.move = function(index) {
    if (this.isValidMove(index)) {
      const last = this.tiles
      const next = this.tiles.slice()
      next[this.hole] = last[index]
      next[index] = last[this.hole]
      this.tiles = next
      this.hole = index
    }
    return this
}

SlidingPuzzle.prototype.shuffle = function() {
  do {
    this.tiles = shuffle(this.tiles)
    this.hole = this.tiles.indexOf(this.size - 1)
  }
  while (!this.isSolvable())
  return this
}

SlidingPuzzle.prototype.isValidMove = function(index) {
  const hole = this.getMatrixPosition(this.hole)
  const tile = this.getMatrixPosition(index)
  return isAdjecentTile(hole, tile)
}

SlidingPuzzle.prototype.isTile = function(index) {
  return index !== this.hole
}

SlidingPuzzle.prototype.isHole = function(index) {
  return index === this.hole
}

SlidingPuzzle.prototype.isSolved = function() {
  return this.tiles.every((tile, index) => tile === index)
}

SlidingPuzzle.prototype.isSolvable = function() {
  if (this.isSolved()) return false
  let product = 1
  for (let i = 1, l = this.size - 1; i <= l; i++) {
    for (let j = i + 1, m = l + 1; j <= m; j++) {
      product *= (this.tiles[i - 1] - this.tiles[j - 1]) / (i - j)
    }
  }
  return product > 0
}

SlidingPuzzle.prototype.getNeigbourIndex = function(index, x, y) {
  const matrix = this.getMatrixPosition(index)
  x += matrix[0]
  y += matrix[1]
  if (x < 0 || x >= this.rows || y < 0 || y >= this.columns) return null
  return x * this.columns + y
}

SlidingPuzzle.prototype.getMatrixPosition = function(index) {
  const row = Math.floor(index / this.columns)
  const column = index % this.columns
  return [row, column]
}

SlidingPuzzle.prototype.getAbsolutePosition = function(index, width = 100, height = width) {
  const [row, column] = this.getMatrixPosition(index)
  const left = row / this.rows * width
  const top = column / this.columns * height
  return [left, top]
}


function init(length) {
  let result = new Array(length)
  while (length--) result[length] = length
  return result
}

function shuffle(array) {
    const result = array.slice()
    var length = array.length
    while (length) {
      const index = getRandomIndex(length--)
      const item = result[length]
      result[length] = result[index]
      result[index] = item
    }
    return result
}

function getRandomIndex(length) {
  return Math.floor(Math.random() * length)
}

function isInteger(value) {
  return typeof value === 'number' && value % 1 === 0
}

function isAdjecentTile(last, next) {
  const x = Math.abs(last[0] - next[0])
  const y = Math.abs(last[1] - next[1])
  return x + y === 1
}

function difference(a, b) {
  return a > b ? a - b : b - a
}
