import React from 'react'
import classNames from 'classnames'

export default class extends React.Component {
  
  constructor() {
    super()
    this.state = {
      focus: false,
      offset: { x: 0, y: 0 },
      position: { x: 0, y: 0 },      
    }
    
    this.setPosition = setPosition.bind(this)
    this.setFocus = setFocus.bind(this)
    
  }

  componentDidUpdate(props) {
    const { puzzle, index, width, height } = this.props
    if (props.index !== index) {
      const [x, y] = puzzle.getAbsolutePosition(index, width, height)
      setTimeout(() => this.setState({ position: { x, y }, focus: false }), 200)
    }
    else this.setPosition(props)
  }
  
  render() {
    
    const { puzzle, move, active, index, value, width, height, image } = this.props
    const { position, offset, focus } = this.state    
    const enabled = active && puzzle.isValidMove(index)
    const className = classNames(puzzle.isTile(index) ? 'tile' : 'hole', { focus })
    
    const scale = 1 - ((enabled & focus)  * 0.05)
    const style = {
      backgroundImage: `url(${image})`,
      backgroundSize: `${puzzle.rows * 100}% ${puzzle.columns * 100}%`,
      backgroundPosition: `-${offset.x}px -${offset.y}px`,
      width: width / puzzle.rows,
      height: height / puzzle.columns,
      transform: `matrix(${scale}, 0, 0, ${scale}, ${position.x}, ${position.y})`,      
    }
    
    return <button
      className={className}
      style={style} 
      onClick={() => move(index)}
      onMouseOver={() => this.setFocus(true)}
      onMouseLeave={() => this.setFocus(false)}
      onFocus={() => this.setFocus(true)}
      onBlur={() => this.setFocus(false)}
      disabled={!enabled}
    >{value + 1}</button>
    
  }
  
}

function setPosition(props) {
  const { puzzle, value, index, width, height } = this.props
  if (props.index !== index || props.width !== width || props.height !== height) {
    const [x, y] = puzzle.getAbsolutePosition(index, width, height)
    this.setState({ 
      position: { x, y },
      size: { width: width / 3, height: height / 3 },
    })
  }
  if (props.width !== width || props.height !== height) {
    const { puzzle, value } = this.props
    const [x, y] = puzzle.getAbsolutePosition(value, width, height)
    this.setState({ offset: { x, y } })
  }
}

function setFocus(value) {
  this.setState({ focus: value })
}
