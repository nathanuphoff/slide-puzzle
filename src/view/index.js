import React from 'react'
import hashids from 'hashids'
import classNames from 'classnames'
import SlidingPuzzle from '../model/SlidingPuzzle'
import Confetti from 'react-confetti'
import Tile from './Tile/'

export default class extends React.Component {
  
  constructor({ rows, columns }) {    
    
    super()  
    this.state = {
      image: null,
      ready: false,
      active: false,
      done: false,
      moves: 0,
      width: 0,
      height: 0,
    }
    
    this.puzzle = new SlidingPuzzle(rows, columns)    
    this.setDimensions = setDimensions.bind(this)
    this.handleKeypress = handleKeypress.bind(this)
    this.startPuzzle = startPuzzle.bind(this)
    this.resetPuzzle = resetPuzzle.bind(this)
    this.moveTile = moveTile.bind(this)
    this.loadImage = loadImage.bind(this)
    
  }
  
  componentWillMount() {
    this.loadImage(image => {
      this.setState({ image, ready: true })
      setTimeout(this.startPuzzle, 1200)
    })   
  }
  
  componentDidMount() {
    this.setDimensions()
    window.addEventListener('keyup', this.handleKeypress)
    window.addEventListener('popstate', this.startPuzzle)
  }
  
  componentWillUnmount() {
    this.setState({ ready: false, active: false })
    window.removeEventListener('keyup', this.handleKeypress)
    window.removeEventListener('popstate', this.startPuzzle)
  }
  
  render() {
    
    const { done, ready, active, image, moves } = this.state
    const className = classNames('board', image && 'image', { ready })
    
    return <div className="slide-puzzle">
      <header>
        <h1>SlidingPuzzle</h1>
      </header>
      <Confetti numberOfPieces={done * 200} width={innerWidth} height={innerHeight}/>
      <section ref='board' className={className}>
        {this.puzzle.tiles.map((value, index) => <Tile 
          key={value} 
          value={value} 
          index={index}
          active={active}
          image={image}
          puzzle={this.puzzle}
          move={this.moveTile}          
          width={this.state.width}
          height={this.state.height}          
        />)}
      </section>
      <footer>        
        <h2 className='counter'><span>{moves}</span> moves</h2>
        <button className='reset' onClick={this.resetPuzzle}>
          <svg viewBox='0 0 32 32'>
            <path className='icon-stroke' d="M8,16 C8,20.418278 11.581722,24 16,24 C20.418278,24 24,20.418278 24,16 C24,11.581722 20.418278,8 16,8 L14,8" stroke="#000" fill="none"/>
            <polygon className='icon-fill' points="16 5 12 8 16 11"/>
          </svg>
          Reset
        </button>
      </footer>
    </div>
    
  }
  
}

function moveTile(index) {
  if (this.puzzle.isValidMove(index)) {
    const { moves } = this.state
    this.puzzle.move(index)
    const done = this.puzzle.isSolved()
    const active = !done
    this.setState({ done, active, moves: moves + 1 })
  }
}

function startPuzzle() {
  const hash = location.hash.slice(1)
  if (this.puzzle.isValidHash(hash)) {
    this.puzzle.load(hash)
  }
  else {
    this.puzzle.shuffle()
    location.hash = this.puzzle.encode()
  }
  this.setState({ active: true, moves: 0, done: false })   
}

function resetPuzzle() {
  this.puzzle.shuffle()
  location.hash = this.puzzle.encode()
  this.startPuzzle()
}

function handleKeypress({ key }) {
  const { hole } = this.puzzle
  switch(key) {
    case 'ArrowLeft': return moveTile.call(this, this.puzzle.getNeigbourIndex(hole, 1, 0))
    case 'ArrowRight': return moveTile.call(this, this.puzzle.getNeigbourIndex(hole, -1, 0))
    case 'ArrowUp': return moveTile.call(this, this.puzzle.getNeigbourIndex(hole, 0, 1))
    case 'ArrowDown': return moveTile.call(this, this.puzzle.getNeigbourIndex(hole, 0, -1))
  }
}

function setDimensions() {
  
  const { rows, columns } = this.puzzle
  const { board } = this.refs  
  const width = board.clientWidth
  const height = board.clientHeight
  
  this.setState({ 
    width: Math.round(width / rows) * rows, 
    height: Math.round(height / columns) * columns,
  })
  
}

function loadImage(callback) {
  const { src } = this.props
  const image = new Image()
  image.onload = event => callback.call(this, src)
  image.onerror = event => {
    if (src) console.warn(`Something went wrong loading ${src}`)
    callback.call(this, null)
  }
  image.src = src
}
